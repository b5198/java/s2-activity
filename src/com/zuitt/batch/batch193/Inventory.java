package com.zuitt.batch.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Inventory {
    public static void main(String[] args){
        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
