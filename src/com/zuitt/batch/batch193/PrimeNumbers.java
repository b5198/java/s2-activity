package com.zuitt.batch.batch193;

import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args){
        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        Scanner appScanner = new Scanner(System.in);
        System.out.println("First 5 prime numbers: Choose a number from 1-5");
        int number = appScanner.nextInt();

        if (number == 1)
            System.out.println("The first prime number is: " + primeNumbers[0]);
        else if (number == 2) {
            System.out.println("The second prime number is: " + primeNumbers[1]);
        }
        else if (number == 3) {
            System.out.println("The third prime number is: " + primeNumbers[2]);
        }
        else if (number == 4) {
            System.out.println("The fourth prime number is: " + primeNumbers[3]);
        }
        else if (number == 5) {
            System.out.println("The fifth prime number is: " + primeNumbers[4]);
        }
        else
            main(null);
    }
}
