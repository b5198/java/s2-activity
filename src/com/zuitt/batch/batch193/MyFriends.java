package com.zuitt.batch.batch193;

import java.util.Arrays;

public class MyFriends {
    public static void main(String[] args){
        String friendArray[] = new String[5];
        friendArray[0] = "Yeji";
        friendArray[1] = "Ryujin";
        friendArray[2] = "Chae Ryeong";
        friendArray[3] = "Yuna";
        friendArray[4] = "Lia";

        Arrays.sort(friendArray);
        System.out.println("My friends are: " + Arrays.toString(friendArray));
    }
}
